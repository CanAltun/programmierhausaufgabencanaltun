package view;

import java.awt.*;
import java.util.LinkedList;

import javax.swing.*;
import controller.BunteRechteckeController;
import model.Rechteck;

public class ZeichenFlaeche extends JPanel
{
    final BunteRechteckeController cntr;

    public ZeichenFlaeche(BunteRechteckeController controller1)
    {
        cntr = controller1;
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        LinkedList<Rechteck> rechteecke = cntr.getRechtecke();

        for (int i = 0; i < rechteecke.size(); i++)
        {
            g.setColor(Color.black);
            g.drawRect(rechteecke.get(i).getX(), rechteecke.get(i).getY(), rechteecke.get(i).getBreite(), rechteecke.get(i).getHoehe());
        }
    }
}