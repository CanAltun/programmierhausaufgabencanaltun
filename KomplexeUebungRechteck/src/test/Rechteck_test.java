package test;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.ZeichenFlaeche;

public class Rechteck_test
{
    static Rechteck Rechteck0 = new Rechteck(10, 10, 30, 40);
    static Rechteck  Rechteck1 = new Rechteck(25, 25, 100, 20);
    static Rechteck  Rechteck2 = new Rechteck(260, 10, 200, 100);
    static Rechteck  Rechteck3 = new Rechteck(5, 500, 300, 25);
    static Rechteck  Rechteck4 = new Rechteck(100, 100, 100, 100);

    static Rechteck  Rechteck5 = new Rechteck();
    static Rechteck  Rechteck6 = new Rechteck();
    static Rechteck  Rechteck7 = new Rechteck();
    static Rechteck  Rechteck8 = new Rechteck();
    static Rechteck  Rechteck9 = new Rechteck();

    static Rechteck  Rechteck10 = new Rechteck(-4,-5,-50,-200);
    static Rechteck  Rechteck11 = new Rechteck();
    static Rechteck  Rechteck12 = new Rechteck();

    static BunteRechteckeController controller0 = new BunteRechteckeController();

    static ZeichenFlaeche z1 = new ZeichenFlaeche(controller0);

    public static void main(String[] args)
    {
    	Rechteck5.setX(200);
         Rechteck5.setY(200);
         Rechteck5.setBreite(200);
         Rechteck5.setHoehe(200);

         Rechteck6.setX(800);
         Rechteck6.setY(400);
         Rechteck6.setBreite(20);
         Rechteck6.setHoehe(20);

         Rechteck7.setX(800);
         Rechteck7.setY(450);
         Rechteck7.setBreite(20);
         Rechteck7.setHoehe(20);

         Rechteck8.setX(850);
         Rechteck8.setY(400);
         Rechteck8.setBreite(20);
         Rechteck8.setHoehe(20);

         Rechteck9.setX(855);
         Rechteck9.setY(455);
         Rechteck9.setBreite(25);
         Rechteck9.setHoehe(25);

         Rechteck11.setX(-10);
         Rechteck11.setY(-10);
         Rechteck11.setBreite(-200);
         Rechteck11.setHoehe(-100);

     
        System.out.println(Rechteck0.toString());

        controller0.add(Rechteck0);
        controller0.add(Rechteck1);
        controller0.add(Rechteck2);
        controller0.add(Rechteck3);
        controller0.add(Rechteck4);
        controller0.add(Rechteck5);
        controller0.add(Rechteck6);
        controller0.add(Rechteck7);
        controller0.add(Rechteck8);
        controller0.add(Rechteck9);

        System.out.println(controller0.toString());

        System.out.println("");
        System.out.println(Rechteck10.toString());
        System.out.println(Rechteck11.toString());
        //notizen//
    }
}