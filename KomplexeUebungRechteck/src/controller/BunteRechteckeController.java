package controller;

import java.util.*;
import model.Rechteck;

public class BunteRechteckeController {

	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		Rechteck[] rechtecke = new Rechteck[anzahl];
		for (int i = 0; i < anzahl; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
		}
	}

	static LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();

	public static void main(String[] args) {

	}

	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck_new) {
		rechtecke.add(rechteck_new);
	}

	public void reset() {
		rechtecke.clear();
	}

	public static LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
}