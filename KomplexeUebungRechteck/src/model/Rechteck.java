package model;
import java.util.Random;

public class Rechteck
{
	Punkt p;
	int breite;
	int hoehe;

	public Rechteck()
	{
	}

	public Rechteck(int x, int y, int breite, int hoehe)
	{
		p = new Punkt();
		this.p.setX(x);
		this.p.setY(y);
		this.breite = mathabs(breite);
		this.hoehe = mathabs(hoehe);
	}
	
	
	public static Rechteck generiereZufallsRechteck() {
		Random random = new Random();
		int x = random.nextInt(1200);
		int y = random.nextInt(1000);
		int breite = random.nextInt(1200-x);
		int hoehe = random.nextInt(1000-y);
		Rechteck rechteck = new Rechteck(x, y, breite, hoehe);
		return rechteck;
	}
	
	public boolean enthealt (Rechteck rechteck) {
		if (rechteck.p.getX() >= this.p.getX() && rechteck.p.getX() <= this.p.getX() + breite) {
			
			if (rechteck.p.getY() >= this.p.getY() && rechteck.p.getY() <= this.p.getY() + hoehe) {
				return true;
		 }
			return false;
		}
		return false;
	}
	
	public boolean enthealt (int x, int y ) {
		if (x >= p.getX() && x <= p.getX() + breite) {
			
			if (y >= p.getY() && x <= p.getY() + hoehe) {
				return true;
		 }
			return false;
		}
		return false;
	}
	public boolean enthealt (Punkt p) {
		if (p.getX() >= this.p.getX() && p.getX() <= this.p.getX() + breite) {
			
			if (p.getY() >= this.p.getY() && p.getY() <= this.p.getY() + hoehe) {
				return true;
		 }
			return false;
		}
		return false;
	}
	public int getX()
	{
		return p.getX();
	}

	public void setX(int x)
	{
		p.setX(x);
	}

	public int getY()
	{
		return p.getY();
	}

	public void setY(int y)
	{
		this.p.setY(y);;
	}

	public int getBreite()
	{
		return breite;
	}

	public void setBreite(int breite)
	{
		this.breite = mathabs(breite);
	}

	public int getHoehe()
	{
		return hoehe;
	}

	public void setHoehe(int hoehe)
	{
		this.hoehe = mathabs(hoehe);
	}
	
	public int mathabs(int wert)
	{
		return Math.abs(wert);
	}

	@Override
	public String toString()
	{
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
}